import Vue from 'vue';
export const EventBus = new Vue({
  methods: {
    emitActiveServer( server ) {
      this.$emit( 'activeServerSet', server );
    },
    emitServerStatusChange( server ) {
      this.$emit( 'serverStatusChange', server );
    }

  }
})
